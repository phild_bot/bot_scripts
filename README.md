# bot_scripts

A collection utility scripts for bot.

- [wifi_bridge](wifi_bridge.sh) sets up a raspberry pi as a wifi bridge. Taken from [here](https://www.willhaley.com/blog/raspberry-pi-wifi-ethernet-bridge/).